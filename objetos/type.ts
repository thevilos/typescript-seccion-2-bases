(() => {

  type Hero = {
    name: string;
    age: number;
    powers: number [];
    getName?: () => string;
  }

  let flash: Hero  = {
    name: 'Barry Allen',
    age: 26,
    powers: [1, 2],
  };

  let superMan: Hero = {
    name: 'Clark Kent',
    age: 45,
    powers: [1],
    getName() {
      return this.name;
    }
  };

})()
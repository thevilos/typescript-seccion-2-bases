(() => {

  let flash: { name: string, age?: number, powers: string[], getName?: () => string }  = {
    name: 'Barry Allen',
    age: 26,
    powers: ['Super velocidad', 'Viajar en el tiempo'],
  };

  let superMan: { name: string, age?: number, powers: string[], getName?: () => string }  = {
    name: 'Clark Kent',
    age: 45,
    powers: ['Super velocidad'],
  };

  console.log(flash);

  // flash = {
  //   name: 'Clark Kent',
  //   age: 40,
  //   powers: ['Super fuerza'],
  //   getName() {
  //     return this.name;
  //   }
  // }

  //console.log(flash.getName());

})()
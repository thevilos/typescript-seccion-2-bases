(() => {

  type Hero = {
    name: string;
    age: number;
    powers: number [];
    getName?: () => string;
  }

  let myCustomVariable: string | number | Hero = 'Edgard';
  console.log(typeof myCustomVariable);

  myCustomVariable = 20;
  console.log(typeof myCustomVariable);

  myCustomVariable = {
    age: 35,
    name: 'Flash',
    powers: [2]
  };
  console.log(typeof myCustomVariable);

})()
(() => {

  interface Client {
    name: string;
    age?: number;
    address: Adress;
    getFullAdress: (id: string) => string;
  }

  interface Adress {
    id: number;
    zip: string;
    city: string;
  }

  const client: Client = {
    name: 'Edgard',
    age: 32,
    address:  {
      id: 122,
      zip: '123456',
      city: 'Ottawa',
    },
    getFullAdress(id: string) {
      return this.address.city;
    }
  }

  const client2: Client = {
    name: 'Melissa',
    age: 26,
    address: {
      city: '',
      id: 1,
      zip: '',
    },
    getFullAdress(id: string) {
      return this.address.city;
    }
  }

})()
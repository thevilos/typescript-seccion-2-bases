(() => {
  type Avengers = {
    nick: string;
    iroman: string;
    vision: string;
    activo: boolean;
    poder: number;
  }

  const avenger: Avengers = {
    nick: 'Samuel L. Jackson',
    iroman: 'Robert downey Jr',
    vision: 'Paul Bettany',
    activo: true,
    poder: 1500.456467,
  }

  // const { poder, vision } = avenger;
  // console.log(poder.toFixed(2), vision.toUpperCase())

  const printAvenger = ({ iroman, ...resto }: Avengers) => {
    console.log(iroman, resto)
  }
  printAvenger(avenger);

  const avengersArr: [string, boolean, boolean] = ['Cap. ameérica', true, false];


  const [cap, iroman, seriaUnNumero] = avengersArr;
  //console.log({iroman, cap, seriaUnNumero})

})()